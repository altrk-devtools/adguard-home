# adguard-home

Standalone AdBlocker plus local DNS server and more.


## NGINX proxy-pass config

```
    # AdGuard Home reverse proxy setup
    location /agh/ {
        proxy_pass		http://127.0.0.1:9500/;
        proxy_redirect		/ /agh/;
        proxy_cookie_path	/ /agh/;

        include             	/etc/nginx/proxy_params.conf;
    }
```
